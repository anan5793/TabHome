package com.hzn.home_with_tabs;

/**
 * Created by hzn on 14-11-25.
 */

interface IconAdapter {
    int getIcon(final int position);
}
