package com.hzn.home_with_tabs;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

/**
 * Activity with tabbed pages
 *
 * @param <V>
 */
public abstract class PagesActivity<V extends PagerAdapter & IconAdapter> extends ActionBarActivity
        implements TabHost.OnTabChangeListener, TabHost.TabContentFactory,
        ViewPager.OnPageChangeListener {

    /**
     * View pager
     */
    protected ViewPager pager;

    /**
     * Tab host
     */
    protected TabHost host;

    /**
     * Pager adapter
     */
    protected V adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pager_with_tabs);

        pager = (ViewPager) findViewById(R.id.vp_pages);
        pager.setOnPageChangeListener(this);
        host = (TabHost) findViewById(R.id.th_tabs);
        host.setup();
        host.setOnTabChangedListener(this);

        configureTabPager();
    }

    /**
     * Configure tabs and pager
     */
    protected void configureTabPager() {
        if (adapter == null) {
            createPager();
            createTabs();
        }
    }

    private void createPager() {
        adapter = createAdapter();
        supportInvalidateOptionsMenu();
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(adapter.getCount());
    }

    /**
     * Create tab using information from current adapter
     * <p/>
     * This can be called when the tabs changed but must be called after an
     * initial call to {@link #configureTabPager()}
     */
    protected void createTabs() {
        if (host.getTabWidget().getTabCount() > 0) {
            // Crash on Gingerbread if tab isn't set to zero since adding a
            // new tab removes selection state on the old tab which will be
            // null unless the current tab index is the same as the first
            // tab index being added
            host.setCurrentTab(0);
            host.clearAllTabs();
        }

        LayoutInflater inflater = getLayoutInflater();
        int count = adapter.getCount();
        for (int i = 0; i < count; i++) {
            TabHost.TabSpec spec = host.newTabSpec("tab" + i);
            spec.setContent(this);
            View view = inflater.inflate(R.layout.tab, null);
            ((ImageView) view.findViewById(R.id.iv_icon)).setImageResource(getIcon(i));
            ((TextView) view.findViewById(R.id.tv_tab)).setText(getTitle(i));

            spec.setIndicator(view);
            host.addTab(spec);
        }
    }

    /**
     * Get title for position
     *
     * @param position
     * @return title
     */
    protected String getTitle(final int position) {
        return adapter.getPageTitle(position).toString();
    }

    /**
     * Get icon for position
     *
     * @param position
     * @return icon
     */
    protected int getIcon(final int position) {
        return adapter.getIcon(position);
    }


    /**
     * Create pager adapter
     *
     * @return pager adapter
     */
    protected abstract V createAdapter();

    @Override
    public void onTabChanged(String tabId) {
        int newPosition = host.getCurrentTab();
        if (newPosition > -1 && newPosition < adapter.getCount()) {
            pager.setCurrentItem(newPosition);
        }
    }

    @Override
    public View createTabContent(String tag) {
        View view = new View(getApplication());
        view.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        host.setCurrentTab(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
