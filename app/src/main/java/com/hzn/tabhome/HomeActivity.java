package com.hzn.tabhome;

import com.hzn.home_with_tabs.PagesActivity;
import com.hzn.home_with_tabs.PagesAdapter;

public class HomeActivity extends PagesActivity<PagesAdapter> {


    @Override
    protected PagesAdapter createAdapter() {
        return new HomePagerAdapter(this);
    }
}
