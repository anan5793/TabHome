package com.hzn.tabhome;

import android.support.v4.app.FragmentActivity;

import com.hzn.home_with_tabs.*;

/**
 * Created by hzn on 14-11-25.
 */
public class HomePagerAdapter extends PagesAdapter {
    public HomePagerAdapter(FragmentActivity activity) {
        super(activity);
    }

    @Override
    public void addPages() {
        addPage(BlankForTestFragment.class, R.drawable.ic_launcher, "hello1");
        addPage(BlankForTestFragment.class, R.drawable.ic_launcher, "hello2");
        addPage(BlankForTestFragment.class, R.drawable.ic_launcher, "hello3");
    }
}
